#!/bin/sh

version=1.11
set -x
aclocal-$version
autoconf
libtoolize
automake-$version --add-missing --foreign

./configure $*
